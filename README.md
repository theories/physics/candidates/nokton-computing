# theories/physics/candidates/nokton-computing

## About this project

Nokton computing is a fast application for calculating some observables.

## Contributing

See [CONTRIBUTING.md](CONTRIBUTING.md) file for more details.

## Licenses

Any project content in this project is under [**Creative Commons LICENSE**](LICENSE.txt) and any project code content in this project is under [**MIT LICENSE**](LICENSE-CODE.txt).