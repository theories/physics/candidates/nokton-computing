#ifndef ARGUMENTS_ARGUMENTS_H
#define ARGUMENTS_ARGUMENTS_H

#include "fetching.h"

ArgumentsFetching arguments_fetch(const int argc, const char **argv, char *configFile, char *outputFile, char *error);

#endif