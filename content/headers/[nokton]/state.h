#ifndef NOKTON_STATE_H
#define NOKTON_STATE_H

typedef struct
{
    int XPosition;
    int YPosition;
    int ZPosition;
    double XPositivePulse;
    double XNegativePulse;
    double YPositivePulse;
    double YNegativePulse;
    double ZPositivePulse;
    double ZNegativePulse;
    double NullPulse;
} NoktonState;

#endif