#ifndef NOKTON_TYPE_H
#define NOKTON_TYPE_H

typedef enum
{
    NOKTON_TYPE_NEUTRAL = 0,
    NOKTON_TYPE_POSITIVE = 1,
    NOKTON_TYPE_NEGATIVE = -1
} NoktonType;

#endif