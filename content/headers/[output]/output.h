#ifndef OUTPUT_OUTPUT_H
#define OUTPUT_OUTPUT_H

#include "writing.h"
#include "../[system]/infos.h"
#include "../[nokton]/state.h"
#include "../[observables]/computing-state.h"

OutputWriting output_write(const char *outputFile, const SystemInfos *systemInfos, const int width, const int kernelsCount, const NoktonState *initialNoktonStates, const ObservablesComputingState *observablesComputingState, char *error);

#endif