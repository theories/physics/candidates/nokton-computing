#ifndef SYSTEM_H
#define SYSTEM_H

#include "initialization.h"
#include "computing.h"
#include "infos.h"
#include "../[observables]/computing-state.h"
#include "../[nokton]/state.h"

SystemInitialization system_initialize_infos(const char *configFile, SystemInfos *systemInfos, NoktonState *initialNoktonStates, int *width, int *kernelsCount, char *error);
void system_initialize_observables_computing_state(const int count, ObservablesComputingState *computingState);
SystemComputing system_compute(const char *configFile, const char *outputFile, char *error);

#endif