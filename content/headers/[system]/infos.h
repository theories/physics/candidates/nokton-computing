#ifndef SYSTEM_INFOS_H
#define SYSTEM_INFOS_H

#include "dimension.h"
#include "type.h"
#include "pulse-type.h"
#include "../(...)/constants.h"
#include "../[nokton]/type.h"
#include "../[config]/observable-states.h"

typedef struct
{
    SystemDimension Dimension;
    SystemType SystemType;
    SystemPulseType PulseType;
    int NoktonsCount;
    NoktonType NoktonTypes[MAX_NOKTONS_COUNT];
    ConfigObservableStates ObservableStates;
} SystemInfos;

#endif