#ifndef SYSTEM_DIMENSION_H
#define SYSTEM_DIMENSION_H

typedef enum
{
    SYSTEM_DIMENSION_1D,
    SYSTEM_DIMENSION_2D,
    SYSTEM_DIMENSION_3D
} SystemDimension;

#endif