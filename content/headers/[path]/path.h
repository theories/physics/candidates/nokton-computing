#ifndef PATH_H
#define PATH_H

#include "nokton-displacements.h"
#include "../(...)/constants.h"

typedef struct
{
    PathNoktonDisplacements NoktonDisplacements[MAX_NOKTONS_COUNT];
} Path;

#endif