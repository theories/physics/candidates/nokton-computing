#ifndef PATH_NOKTON_DISPLACEMENTS_H
#define PATH_NOKTON_DISPLACEMENTS_H

#include "displacement.h"
#include "../(...)/constants.h"

typedef struct
{
    PathDisplacement Displacements[MAX_WIDTH];
} PathNoktonDisplacements;

#endif