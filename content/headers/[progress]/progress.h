#ifndef PROGRESS_PROGRESS_H
#define PROGRESS_PROGRESS_H

#include "progress-step-type.h"
#include "progress-infos.h"

void progress_show(ProgressStepType stepType, ProgressInfos *progressInfos);

#endif