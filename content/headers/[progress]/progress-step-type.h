#ifndef PROGRESS_STEP_TYPE_H
#define PROGRESS_STEP_TYPE_H

typedef enum
{
    PROGRESS_STEP_TYPE_STARTING,
    PROGRESS_STEP_TYPE_COMPUTING,
    PROGRESS_STEP_TYPE_ENDING
} ProgressStepType;

#endif