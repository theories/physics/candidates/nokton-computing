#ifndef PROGRESS_INFOS_H
#define PROGRESS_INFOS_H

#include <time.h>

typedef struct
{
    int KernelsCount;
    int CurrentKernel;
    long KernelTotalPaths;
    long KernelCurrentPath;
    time_t Start;
    time_t StepStart;
    time_t StepEnd;
    time_t End;
} ProgressInfos;

#endif