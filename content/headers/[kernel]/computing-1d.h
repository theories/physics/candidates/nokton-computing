#ifndef KERNEL_COMPUTING_1D_H
#define KERNEL_COMPUTING_1D_H

#include "distance.h"
#include "../[path]/path.h"
#include "../[path]/displacement.h"
#include "../[path]/nokton-displacements.h"
#include "../[nokton]/state.h"
#include "../[system]/infos.h"
#include "../[system]/pulse-type.h"
#include "../[kernel]/nokton-contribution.h"
#include "../[kernel]/path-computing-state.h"

void kernel_compute1d_position(const PathDisplacement displacement, NoktonState *noktonState);
void kernel_compute1d_positions(const int time, const int count, const PathNoktonDisplacements *noktonPaths, NoktonState *noktonStates);

void kernel_compute1d_distances(const NoktonState *noktonState1, const NoktonState *noktonState2, KernelDistance *distance);

void kernel_compute1d_gravitational_contribution(const int noktonIndex, const int count, const NoktonState *noktonStates, KernelNoktonContribution *noktonContribution);
void kernel_compute1d_gravitational_contributions(const int count, const NoktonState *noktonStates, KernelNoktonContribution *noktonContributions);

void kernel_compute1d_electric_contribution(const SystemInfos *systemInfos, const int noktonIndex, const NoktonState *noktonStates, KernelNoktonContribution *noktonContribution);
void kernel_compute1d_electric_contributions(const SystemInfos *systemInfos, const NoktonState *noktonStates, KernelNoktonContribution *noktonContributions);

void kernel_compute1d_pulse(const SystemPulseType pulseType, const KernelNoktonContribution *noktonContribution, NoktonState *noktonState);
void kernel_compute1d_pulses(const SystemInfos *systemInfos, const KernelNoktonContribution *noktonContributions, NoktonState *noktonStates);

double kernel_compute1d_path_probability(const PathDisplacement displacement, const NoktonState *noktonState);
double kernel_compute1d_path_probabilities(const int time, const int count, const PathNoktonDisplacements *noktonPaths, const NoktonState *noktonStates);

KernelPathComputingState kernel_compute1d_next_path(const SystemPulseType pulseType, const int width, const int noktonIndex, const int time, Path *path);

#endif