#ifndef KERNEL_COMPUTING_STATE_H
#define KERNEL_COMPUTING_STATE_H

#include "nokton-contribution.h"
#include "../(...)/constants.h"
#include "../[nokton]/state.h"

typedef struct
{
    NoktonState NoktonStates[MAX_NOKTONS_COUNT];
    KernelNoktonContribution NoktonContributions[MAX_NOKTONS_COUNT];
    double PathProbability;
} KernelComputingState;

#endif