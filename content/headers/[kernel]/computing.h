#ifndef KERNEL_COMPUTING_H
#define KERNEL_COMPUTING_H

#include "distance.h"
#include "../[path]/path.h"
#include "../[path]/displacement.h"
#include "../[path]/nokton-displacements.h"
#include "../[nokton]/state.h"
#include "../[system]/infos.h"
#include "../[system]/pulse-type.h"
#include "../[kernel]/computing-state.h"
#include "../[kernel]/path-computing-state.h"

void kernel_compute_positions(const SystemInfos *systemInfos, const int time, const PathNoktonDisplacements *noktonPaths, NoktonState *noktonStates);
int kernel_compute_distance(const int position1, const int position2);
void kernel_compute_gravitational_contributions(const SystemInfos *systemInfos, const NoktonState *noktonStates, KernelNoktonContribution *noktonContributions);
void kernel_compute_electric_contributions(const SystemInfos *systemInfos, const NoktonState *noktonStates, KernelNoktonContribution *noktonContributions);
void kernel_compute_contributions(const SystemInfos *systemInfos, const NoktonState *noktonStates, KernelNoktonContribution *noktonContributions);
void kernel_compute_pulses(const SystemInfos *systemInfos, const KernelNoktonContribution *noktonContributions, NoktonState *noktonStates);
double kernel_compute_path_probabilities(const SystemInfos *systemInfos, const int time, const PathNoktonDisplacements *noktonPaths, const NoktonState *noktonStates);
void kernel_compute_state(const SystemInfos *systemInfos, const int time, const PathNoktonDisplacements *noktonPaths, KernelComputingState *kernelComputingState);
void kernel_compute_path_state(const SystemInfos *systemInfos, const int width, const Path *path, KernelComputingState *kernelComputingState);
KernelPathComputingState kernel_compute_next_path(const SystemInfos *systemInfos, const int width, const int noktonIndex, const int time, Path *path);

#endif