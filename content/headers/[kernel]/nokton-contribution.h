#ifndef KERNAL_NOKTON_CONTRIBUTION_H
#define KERNAL_NOKTON_CONTRIBUTION_H

typedef struct
{
    double XPositive;
    double XNegative;
    double YPositive;
    double YNegative;
    double ZPositive;
    double ZNegative;
} KernelNoktonContribution;

#endif