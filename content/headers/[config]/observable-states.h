#ifndef CONFIG_OBSERVABLE_STATES_H
#define CONFIG_OBSERVABLE_STATES_H

#include "observable-state.h"

typedef struct
{
    ConfigObservableState FinalPosition;
    ConfigObservableState FinalPulse;
    ConfigObservableState FinalMass;
} ConfigObservableStates;

#endif