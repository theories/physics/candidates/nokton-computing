#ifndef CONFIG_CONFIG_H
#define CONFIG_CONFIG_H

#include <stdio.h>
#include "fetching.h"
#include "../[system]/infos.h"
#include "../[nokton]/state.h"

ConfigFetching config_read_line(FILE *file, const int maxLineLength, char *line);
ConfigFetching config_is_config(const char *line, const int maxLineLength);
ConfigFetching config_get_key_value(const char *line, const int maxLineLength, char *key, char *value);
void config_trim_line_string(const char *s, char *trimString);
ConfigFetching config_convert_to_int(const char *s, int *value);
ConfigFetching config_convert_to_double(const char *s, double *value);
ConfigFetching config_initialize_position(const char *s, int *position);
ConfigFetching config_initialize_pulse(const char *s, double *pulse, char *error);
ConfigFetching config_initialize_observables(const char *observables, const int maxObservablesLength, SystemInfos *systemInfos);
ConfigFetching config_initialize_properties(const char *key, const char *value, SystemInfos *systemInfos, int *width, int *kernelsCount, char *error);
ConfigFetching config_initialize_nokton(const char *key, const char *value, NoktonState *noktonState, char *error);

#endif