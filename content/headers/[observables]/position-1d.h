#ifndef OBSERVABLES_POSITION_1D_H
#define OBSERVABLES_POSITION_1D_H

#include "../[kernel]/computing-state.h"
#include "../[observables]/final-position.h"

void observables_compute1d_final_position_mean(const int count, const KernelComputingState *state, ObservablesFinalPosition *means);
void observables_compute1d_final_position_stddev(const int count, const KernelComputingState *state, ObservablesFinalPosition *stddevs);
void observables_finalize1d_final_position_stddev(const int count, const ObservablesFinalPosition *means, ObservablesFinalPosition *stddevs);

#endif