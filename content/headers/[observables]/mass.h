#ifndef OBSERVABLES_MASS_H
#define OBSERVABLES_MASS_H

#include "../[kernel]/computing-state.h"
#include "../[system]/infos.h"
#include "../[path]/path.h"

void observables_compute_final_mass_mean(const SystemInfos *systemInfos, const int width, const KernelComputingState *state, const Path *path, double *means);

#endif