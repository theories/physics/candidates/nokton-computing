#ifndef OBSERVABLES_POSITION_H
#define OBSERVABLES_POSITION_H

#include "../[kernel]/computing-state.h"
#include "../[observables]/final-position.h"
#include "../[system]/infos.h"

void observables_compute_final_position_mean(const SystemInfos *systemInfos, const KernelComputingState *state, ObservablesFinalPosition *means);
void observables_compute_final_position_stddev(const SystemInfos *systemInfos, const KernelComputingState *state, ObservablesFinalPosition *stddevs);
void observables_finalize_final_position_stddev(const SystemInfos *systemInfos, const ObservablesFinalPosition *means, ObservablesFinalPosition *stddevs);

#endif