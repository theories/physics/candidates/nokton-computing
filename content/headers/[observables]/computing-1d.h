#ifndef OBSERVABLES_COMPUTING_1D_H
#define OBSERVABLES_COMPUTING_1D_H

#include "../[kernel]/computing-state.h"
#include "../[nokton]/state.h"

void observables_initialize1d_path_computing(const int count, const NoktonState *initialNoktonStates, KernelComputingState *kernelComputingState);

#endif