#ifndef OBSERVABLES_COMPUTING_H
#define OBSERVABLES_COMPUTING_H

#include "../[system]/infos.h"
#include "../[path]/path.h"
#include "../[kernel]/computing-state.h"
#include "../[observables]/computing-state.h"
#include "../[progress]/progress-infos.h"

void observables_kernel_compute(const SystemInfos *systemInfos, const int kernelsCount, const int width, const Path *path, KernelComputingState *kernelComputingState, ObservablesComputingState *observablesComputingState);
void observables_kernel_finalize(const SystemInfos *systemInfos, ObservablesComputingState *observablesComputingState);
void observables_compute_initial_nokton_states(const SystemInfos *systemInfos, const int width, const int kernelIndex, const ObservablesComputingState *observablesComputingState, const NoktonState *initialNoktonStates, NoktonState *newInitialNoktonStates, Path *path);
void observables_initialize_path_computing(const SystemInfos *systemInfos, const NoktonState *initialNoktonStates, KernelComputingState *kernelComputingState);
void observables_compute(const SystemInfos *systemInfos, const int width, const int kernelsCount, const NoktonState *initialNoktonStates, ObservablesComputingState *observablesComputingState, ProgressInfos *progressInfos);

#endif