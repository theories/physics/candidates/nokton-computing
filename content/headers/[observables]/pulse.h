#ifndef OBSERVABLES_PULSE_H
#define OBSERVABLES_PULSE_H

#include "../[kernel]/computing-state.h"
#include "../[observables]/final-pulse.h"
#include "../[system]/infos.h"

void observables_compute_final_pulse_mean(const SystemInfos *systemInfos, const KernelComputingState *state, ObservablesFinalPulse *means);

#endif