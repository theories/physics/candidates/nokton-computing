#ifndef OBSERVABLES_MASS_1D_H
#define OBSERVABLES_MASS_1D_H

#include "../[kernel]/computing-state.h"
#include "../[path]/path.h"

void observables_compute1d_final_mass_mean(const int count, const int width, const KernelComputingState *state, const Path *path, double *means);

#endif