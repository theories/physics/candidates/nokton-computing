#ifndef OBSERVABLES_FINAL_POSITION_H
#define OBSERVABLES_FINAL_POSITION_H

typedef struct
{
    double XPosition;
    double YPosition;
    double ZPosition;
} ObservablesFinalPosition;

#endif