#ifndef OBSERVABLES_FINAL_PULSE_H
#define OBSERVABLES_FINAL_PULSE_H

typedef struct
{
    double XPositivePulse;
    double XNegativePulse;
    double YPositivePulse;
    double YNegativePulse;
    double ZPositivePulse;
    double ZNegativePulse;
    double NullPulse;
} ObservablesFinalPulse;

#endif