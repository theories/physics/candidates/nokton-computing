#ifndef OBSERVABLES_COMPUTING_STATE_H
#define OBSERVABLES_COMPUTING_STATE_H

#include "final-position.h"
#include "final-pulse.h"
#include "../(...)/constants.h"

typedef struct
{
    ObservablesFinalPosition FinalPositionMeans[MAX_NOKTONS_COUNT];
    ObservablesFinalPosition FinalPositionStddevs[MAX_NOKTONS_COUNT];
    ObservablesFinalPulse FinalPulseMeans[MAX_NOKTONS_COUNT];
    double FinalMassMeans[MAX_NOKTONS_COUNT];
} ObservablesComputingState;

#endif