#ifndef OBSERVABLES_PULSE_1D_H
#define OBSERVABLES_PULSE_1D_H

#include "../[kernel]/computing-state.h"
#include "../[observables]/final-pulse.h"

void observables_compute1d_final_pulse_mean(const int count, const KernelComputingState *state, ObservablesFinalPulse *means);

#endif