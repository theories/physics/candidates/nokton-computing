#include <stdio.h>
#include "../../headers/(...)/constants.h"
#include "../../headers/[progress]/progress.h"

void progress_show(ProgressStepType stepType, ProgressInfos *progressInfos)
{
    switch (stepType)
    {
    case PROGRESS_STEP_TYPE_STARTING:
        printf("============================================\n");
        printf("Kernels : %d - Paths : %ld\n", progressInfos->KernelsCount, progressInfos->KernelTotalPaths);
        break;
    case PROGRESS_STEP_TYPE_ENDING:
        printf("Total time : %f(m)\n", difftime(progressInfos->End, progressInfos->Start) / 60);
        printf("============================================\n");
        break;
    default:
    {
        if (progressInfos->KernelCurrentPath % PROGRESS_STEP == 0)
        {
            double ratio = progressInfos->KernelCurrentPath / (double)progressInfos->KernelTotalPaths;

            time(&progressInfos->StepEnd);
            printf(
                "Kernels : %d/%d (%.1f%%) - Paths : %ld/%ld (%0.4f%%) - K. rem. time : %f(m)\n",
                progressInfos->CurrentKernel,
                progressInfos->KernelsCount,
                100 * progressInfos->CurrentKernel / (float)progressInfos->KernelsCount,
                progressInfos->KernelCurrentPath,
                progressInfos->KernelTotalPaths,
                100 * ratio,
                difftime(progressInfos->StepEnd, progressInfos->Start) * (1 - ratio) / (60 * ratio));

            progressInfos->StepStart = progressInfos->StepEnd;
        }
    }
    break;
    }
}