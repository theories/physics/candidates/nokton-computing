#include <string.h>
#include <stdlib.h>
#include "../../headers/[config]/config.h"

ConfigFetching config_read_line(FILE *file, const int maxLineLength, char *line)
{
    int c;

    for (int i = 0; i < maxLineLength; i++)
    {
        c = getc(file);

        if (c != '\n' && c != EOF)
        {
            line[i] = c;
        }
        else
        {
            line[i] = '\0';

            if (c == '\n')
            {
                return CONFIG_FETCHING_MORE_LINES;
            }
            else
            {
                return CONFIG_FETCHING_NO_MORE_LINES;
            }
        }
    }

    return CONFIG_FETCHING_NO_MORE_LINES;
}

ConfigFetching config_is_config(const char *line, const int maxLineLength)
{
    char firstChar;

    if (maxLineLength == 0 || (firstChar = line[0]) == '\0' || firstChar != '#')
    {
        return CONFIG_FETCHING_IS_CONFIG;
    }

    return CONFIG_FETCHING_IS_COMMENT;
}

ConfigFetching config_get_key_value(const char *line, const int maxLineLength, char *key, char *value)
{
    char c;
    int valueOffset = -1;

    for (int i = 0; i < maxLineLength; i++)
    {
        c = line[i];

        if (c == '=')
        {
            valueOffset = i + 1;
            key[i] = '\0';
        }
        else
        {
            if (valueOffset != -1)
            {
                value[i - valueOffset] = c;
            }
            else
            {
                key[i] = c;
            }

            if (c == '\0')
            {
                break;
            }
        }

        if (i == maxLineLength - 1)
        {
            if (valueOffset != -1)
            {
                value[i - valueOffset] = '\0';
            }
            else
            {
                key[i] = '\0';
            }
        }
    }

    if (valueOffset == -1)
    {
        return CONFIG_FETCHING_UNFOUND_VALUE;
    }

    return CONFIG_FETCHING_SUCCESS;
}

void config_trim_line_string(const char *s, char *trimString)
{
    int length = strlen(s);

    if (length == 0)
    {
        trimString[0] = '\0';
        return;
    }

    char c;
    int trimStringLength = 0;

    for (int i = 0; i < length; i++)
    {
        c = s[i];

        if (i == length - 1 || (c != ' ' && c != '\t'))
        {
            strcpy(trimString, &s[i]);
            trimStringLength = length - i;
            break;
        }
    }

    for (int i = trimStringLength - 1; i >= 0; i--)
    {
        c = trimString[i];

        if (c != ' ' && c != '\t')
        {
            break;
        }
        else
        {
            trimString[i] = '\0';
        }
    }
}

ConfigFetching config_convert_to_int(const char *s, int *value)
{
    char *endConversion;
    *value = strtol(s, &endConversion, 10);

    if (*endConversion != '\0')
    {
        return CONFIG_FETCHING_UNKNOWN_VALUE;
    }

    return CONFIG_FETCHING_SUCCESS;
}

ConfigFetching config_convert_to_double(const char *s, double *value)
{
    char *endConversion;
    *value = strtod(s, &endConversion);

    if (*endConversion != '\0')
    {
        return CONFIG_FETCHING_UNKNOWN_VALUE;
    }

    return CONFIG_FETCHING_SUCCESS;
}

ConfigFetching config_initialize_position(const char *s, int *position)
{
    int convertedValue;

    if (config_convert_to_int(s, &convertedValue) == CONFIG_FETCHING_UNKNOWN_VALUE)
    {
        return CONFIG_FETCHING_UNKNOWN_VALUE;
    }

    *position = convertedValue;
    return CONFIG_FETCHING_SUCCESS;
}

ConfigFetching config_initialize_pulse(const char *s, double *pulse, char *error)
{
    double convertedValue;

    if (config_convert_to_double(s, &convertedValue) == CONFIG_FETCHING_UNKNOWN_VALUE)
    {
        return CONFIG_FETCHING_UNKNOWN_VALUE;
    }

    if (convertedValue < 0 || convertedValue > 1)
    {
        strcpy(error, "Excepted value between 0 and 1");
        return CONFIG_FETCHING_VALUE_OUT_OF_RANGE;
    }

    *pulse = convertedValue;
    return CONFIG_FETCHING_SUCCESS;
}

ConfigFetching config_initialize_observables(const char *observables, const int maxObservablesLength, SystemInfos *systemInfos)
{
    if (strlen(observables) == 0)
    {
        return CONFIG_FETCHING_UNFOUND_VALUE;
    }

    char c;
    int observableOffset = 0;
    char observable[MAX_CONFIG_LINE_LENGTH];
    char trimObservable[MAX_CONFIG_LINE_LENGTH];

    for (int i = 0; i < maxObservablesLength; i++)
    {
        c = observables[i];

        if (c == '+' || i == maxObservablesLength - 1)
        {
            observable[i - observableOffset] = '\0';
            config_trim_line_string(observable, trimObservable);

            if (strlen(trimObservable) == 0)
            {
                return CONFIG_FETCHING_UNFOUND_VALUE;
            }

            if (strcmp(trimObservable, "FINAL_POSITION") == 0)
            {
                systemInfos->ObservableStates.FinalPosition = CONFIG_OBSERVABLE_STATE_ENABLED;
            }
            else if (strcmp(trimObservable, "FINAL_PULSE") == 0)
            {
                systemInfos->ObservableStates.FinalPulse = CONFIG_OBSERVABLE_STATE_ENABLED;
            }
            else if (strcmp(trimObservable, "FINAL_MASS") == 0)
            {
                systemInfos->ObservableStates.FinalMass = CONFIG_OBSERVABLE_STATE_ENABLED;
            }
            else
            {
                return CONFIG_FETCHING_UNKNOWN_VALUE;
            }

            observableOffset = i + 1;
        }
        else
        {
            observable[i - observableOffset] = c;
        }
    }

    return CONFIG_FETCHING_SUCCESS;
}

ConfigFetching config_initialize_properties(const char *key, const char *value, SystemInfos *systemInfos, int *width, int *kernelsCount, char *error)
{
    // Dimension.
    if (strcmp(key, "Dimension") == 0)
    {
        SystemDimension dimension;

        if (strcmp(value, "1D") == 0)
        {
            dimension = SYSTEM_DIMENSION_1D;
        }
        else if (strcmp(value, "2D") == 0)
        {
            dimension = SYSTEM_DIMENSION_1D;
        }
        else if (strcmp(value, "2D") == 0)
        {
            dimension = SYSTEM_DIMENSION_1D;
        }
        else
        {
            return CONFIG_FETCHING_UNKNOWN_VALUE;
        }

        systemInfos->Dimension = dimension;
        return CONFIG_FETCHING_SUCCESS;
    }
    // System type.
    if (strcmp(key, "SystemType") == 0)
    {
        SystemType systemType;

        if (strcmp(value, "NONE") == 0)
        {
            systemType = SYSTEM_TYPE_NONE;
        }
        else if (strcmp(value, "NO_GRAVITATIONAL_CONTRIBUTIONS") == 0)
        {
            systemType = SYSTEM_TYPE_NO_GRAVITATIONAL_CONTRIBUTIONS;
        }
        else if (strcmp(value, "NO_ELECTRIC_CONTRIBUTIONS") == 0)
        {
            systemType = SYSTEM_TYPE_NO_ELECTRIC_CONTRIBUTIONS;
        }
        else
        {
            return CONFIG_FETCHING_UNKNOWN_VALUE;
        }

        systemInfos->SystemType = systemType;
        return CONFIG_FETCHING_SUCCESS;
    }
    // Pulse type.
    if (strcmp(key, "PulseType") == 0)
    {
        SystemPulseType pulseType;

        if (strcmp(value, "NONE") == 0)
        {
            pulseType = SYSTEM_PULSE_TYPE_NONE;
        }
        else if (strcmp(value, "NO_NULL") == 0)
        {
            pulseType = SYSTEM_PULSE_TYPE_NO_NULL;
        }
        else
        {
            return CONFIG_FETCHING_UNKNOWN_VALUE;
        }

        systemInfos->PulseType = pulseType;
        return CONFIG_FETCHING_SUCCESS;
    }
    // Width.
    int convertedValue;

    if (strcmp(key, "Width") == 0)
    {
        if (config_convert_to_int(value, &convertedValue) == CONFIG_FETCHING_UNKNOWN_VALUE)
        {
            return CONFIG_FETCHING_UNKNOWN_VALUE;
        }

        if (convertedValue < 1 || convertedValue > MAX_WIDTH)
        {
            sprintf(error, "Excepted value betwen 1 and %d", MAX_WIDTH);
            return CONFIG_FETCHING_VALUE_OUT_OF_RANGE;
        }

        *width = convertedValue;
        return CONFIG_FETCHING_SUCCESS;
    }
    // Kernels count.
    if (strcmp(key, "KernelsCount") == 0)
    {
        if (config_convert_to_int(value, &convertedValue) == CONFIG_FETCHING_UNKNOWN_VALUE)
        {
            return CONFIG_FETCHING_UNKNOWN_VALUE;
        }

        if (convertedValue < 1)
        {
            strcpy(error, "Excepted value greater or equal to 1");
            return CONFIG_FETCHING_VALUE_OUT_OF_RANGE;
        }

        *kernelsCount = convertedValue;
        return CONFIG_FETCHING_SUCCESS;
    }
    // Observables.
    if (strcmp(key, "Observables") == 0)
    {
        return config_initialize_observables(value, MAX_CONFIG_LINE_LENGTH - 1, systemInfos);
    }

    return CONFIG_FETCHING_UNKNOWN_KEY;
}

ConfigFetching config_initialize_nokton(const char *key, const char *value, NoktonState *noktonState, char *error)
{
    // X-Position.
    if (strcmp(key, "XNoktonPosition") == 0)
    {
        return config_initialize_position(value, &noktonState->XPosition);
    }
    // Y-Position.
    if (strcmp(key, "YNoktonPosition") == 0)
    {
        return config_initialize_position(value, &noktonState->YPosition);
    }
    // Z-Position.
    if (strcmp(key, "ZNoktonPosition") == 0)
    {
        return config_initialize_position(value, &noktonState->ZPosition);
    }
    // X-Negative Pulse.
    if (strcmp(key, "NoktonXNegPulse") == 0)
    {
        return config_initialize_pulse(value, &noktonState->XNegativePulse, error);
    }
    // X-Positive Pulse.
    if (strcmp(key, "NoktonXPosPulse") == 0)
    {
        return config_initialize_pulse(value, &noktonState->XPositivePulse, error);
    }
    // Y-Negative Pulse.
    if (strcmp(key, "NoktonYNegPulse") == 0)
    {
        return config_initialize_pulse(value, &noktonState->YNegativePulse, error);
    }
    // Y-Positive Pulse.
    if (strcmp(key, "NoktonYPosPulse") == 0)
    {
        return config_initialize_pulse(value, &noktonState->YPositivePulse, error);
    }
    // Z-Negative Pulse.
    if (strcmp(key, "NoktonZNegPulse") == 0)
    {
        return config_initialize_pulse(value, &noktonState->ZNegativePulse, error);
    }
    // Z-Positive Pulse.
    if (strcmp(key, "NoktonZPosPulse") == 0)
    {
        return config_initialize_pulse(value, &noktonState->ZPositivePulse, error);
    }

    return CONFIG_FETCHING_UNKNOWN_KEY;
}