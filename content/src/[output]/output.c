#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "../../headers/[output]/output.h"

OutputWriting output_write(const char *outputFile, const SystemInfos *systemInfos, const int width, const int kernelsCount, const NoktonState *initialNoktonStates, const ObservablesComputingState *observablesComputingState, char *error)
{
    // Open file.
    FILE *file = fopen(outputFile, "w");

    if (file == NULL)
    {
        sprintf(error, "Can not open file '%s' for writing", outputFile);
        return OUTPUT_WRITING_FAILURE;
    }
    // Write output.
    int count = systemInfos->NoktonsCount;
    // Configuration.
    fprintf(file, "============================================\n");

    char enumString[MAX_ENUM_STRING];

    SystemDimension dimension = systemInfos->Dimension;

    switch (dimension)
    {
    case SYSTEM_DIMENSION_1D:
        strcpy(enumString, "1D");
        break;
    case SYSTEM_DIMENSION_2D:
        strcpy(enumString, "2D");
        break;
    default:
        strcpy(enumString, "3D");
        break;
    }

    fprintf(file, "Dimension = %s\n", enumString);

    switch (systemInfos->SystemType)
    {
    case SYSTEM_TYPE_NONE:
        strcpy(enumString, "NONE");
        break;
    case SYSTEM_TYPE_NO_GRAVITATIONAL_CONTRIBUTIONS:
        strcpy(enumString, "NO_GRAVITATIONAL_CONTRIBUTIONS");
        break;
    default:
        strcpy(enumString, "NO_ELECTRIC_CONTRIBUTIONS");
        break;
    }

    fprintf(file, "System type = %s\n", enumString);

    switch (systemInfos->PulseType)
    {
    case SYSTEM_PULSE_TYPE_NONE:
        strcpy(enumString, "NONE");
        break;
    default:
        strcpy(enumString, "NO_NULL");
        break;
    }

    fprintf(file, "Pulse type = %s\n", enumString);

    fprintf(file, "Kernels count = %d\n", kernelsCount);
    fprintf(file, "Width = %d\n", width);
    fprintf(file, "Noktons count = %d\n", count);

    fprintf(file, "============================================\n");

    for (int i = 0; i < count; i++)
    {
        fprintf(file, "============================================\n");
        fprintf(file, "-Nokton #%d-\n\n", i + 1);

        switch (systemInfos->NoktonTypes[i])
        {
        case NOKTON_TYPE_NEUTRAL:
            strcpy(enumString, "NEUTRAL");
            break;
        case NOKTON_TYPE_POSITIVE:
            strcpy(enumString, "POSITIVE");
            break;
        default:
            strcpy(enumString, "NEGATIVE");
            break;
        }

        fprintf(file, "Type = %s\n", enumString);

        NoktonState noktonState = initialNoktonStates[i];
        // Positions.
        switch (dimension)
        {
        case SYSTEM_DIMENSION_1D:
            fprintf(file, "Initial position = %d\n", noktonState.XPosition);
            break;
        case SYSTEM_DIMENSION_2D:
            fprintf(file, "Initial position = (%d, %d)\n", noktonState.XPosition, noktonState.YPosition);
            break;
        default:
            fprintf(file, "Initial position = (%d, %d, %d)\n", noktonState.XPosition, noktonState.YPosition, noktonState.ZPosition);
            break;
        }

        if (systemInfos->ObservableStates.FinalPosition == CONFIG_OBSERVABLE_STATE_ENABLED)
        {
            ObservablesFinalPosition finalPositionMean = observablesComputingState->FinalPositionMeans[i];
            ObservablesFinalPosition finalPositionStddev = observablesComputingState->FinalPositionStddevs[i];

            switch (dimension)
            {
            case SYSTEM_DIMENSION_1D:
                fprintf(file, "<Final position> = %.12f\n", finalPositionMean.XPosition);
                fprintf(file, "<<Final position>> = %.12f\n\n", finalPositionStddev.XPosition);
                break;
            case SYSTEM_DIMENSION_2D:
                fprintf(file, "<Final position> = (%.12f, %.12f)\n", finalPositionMean.XPosition, finalPositionMean.YPosition);
                fprintf(file, "<<Final position>> = (%.12f, %.12f)\n\n", finalPositionStddev.XPosition, finalPositionStddev.YPosition);
                break;
            default:
                fprintf(file, "<Final position> = (%.12f, %.12f, %.12f)\n", finalPositionMean.XPosition, finalPositionMean.YPosition, finalPositionMean.ZPosition);
                fprintf(file, "<<Final position>> = (%.12f, %.12f, %.12f)\n\n", finalPositionStddev.XPosition, finalPositionStddev.YPosition, finalPositionStddev.ZPosition);
                break;
            }
        }
        // Pulses.
        if (systemInfos->ObservableStates.FinalPulse == CONFIG_OBSERVABLE_STATE_ENABLED)
        {
            ObservablesFinalPulse finalPulse = observablesComputingState->FinalPulseMeans[i];

            if (dimension == SYSTEM_DIMENSION_1D || dimension == SYSTEM_DIMENSION_2D || dimension == SYSTEM_DIMENSION_3D)
            {
                fprintf(file, "Initial X-pulses = (%.12f, %.12f)\n", noktonState.XNegativePulse, noktonState.XPositivePulse);
                fprintf(file, "<Final X-pulses> = (%.12f, %.12f)\n\n", finalPulse.XNegativePulse, finalPulse.XPositivePulse);
            }

            if (dimension == SYSTEM_DIMENSION_2D || dimension == SYSTEM_DIMENSION_3D)
            {
                fprintf(file, "Initial Y-pulses = (%.12f, %.12f)\n", noktonState.YNegativePulse, noktonState.YPositivePulse);
                fprintf(file, "<Final Y-Pulse> = (%.12f, %.12f)\n\n", finalPulse.YNegativePulse, finalPulse.YPositivePulse);
            }

            if (dimension == SYSTEM_DIMENSION_3D)
            {
                fprintf(file, "Initial Z-pulses = (%.12f, %.12f)\n", noktonState.ZNegativePulse, noktonState.ZPositivePulse);
                fprintf(file, "<Final Z-Pulse> = (%.12f, %.12f)\n\n", finalPulse.ZNegativePulse, finalPulse.ZPositivePulse);
            }

            if (systemInfos->PulseType != SYSTEM_PULSE_TYPE_NO_NULL)
            {
                fprintf(file, "Initial null pulse = %.12f\n", noktonState.NullPulse);
                fprintf(file, "<Final null pulse> = %.12f\n\n", finalPulse.NullPulse);
            }
        }
        // Masses.
        if (systemInfos->ObservableStates.FinalMass == CONFIG_OBSERVABLE_STATE_ENABLED)
        {
            fprintf(file, "<Final mass> = %.12f\n", observablesComputingState->FinalMassMeans[i]);
        }
    }

    fclose(file);
    return OUTPUT_WRITING_SUCCESS;
}