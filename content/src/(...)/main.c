#include "../../headers/(...)/main.h"
#include "../../headers/(...)/constants.h"
#include "../../headers/[arguments]/arguments.h"
#include "../../headers/[system]/system.h"
#include <stdio.h>
#include <stdlib.h>

int main(const int argc, const char **argv)
{
    char configFile[MAX_FILE_PATH];
    char outputFile[MAX_FILE_PATH];
    char error[MAX_ERROR_LENGTH];

    ArgumentsFetching fetching = arguments_fetch(argc, argv, configFile, outputFile, error);

    if (fetching == ARGUMENTS_FETCHING_FAILURE ||
        (fetching == ARGUMENTS_FETCHING_SUCCESS && system_compute(configFile, outputFile, error) != SYSTEM_COMPUTING_SUCCESS))
    {
        printf("%s\n", error);
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}