#include "../../headers/[kernel]/computing.h"
#include "../../headers/[kernel]/computing-1d.h"

void kernel_compute_positions(const SystemInfos *systemInfos, const int time, const PathNoktonDisplacements *noktonPaths, NoktonState *noktonStates)
{
    switch (systemInfos->Dimension)
    {
    case SYSTEM_DIMENSION_1D:
        kernel_compute1d_positions(time, systemInfos->NoktonsCount, noktonPaths, noktonStates);
        break;
    default:
        break;
    }
}

int kernel_compute_distance(const int position1, const int position2)
{
    return position1 - position2;
}

void kernel_compute_gravitational_contributions(const SystemInfos *systemInfos, const NoktonState *noktonStates, KernelNoktonContribution *noktonContributions)
{
    switch (systemInfos->Dimension)
    {
    case SYSTEM_DIMENSION_1D:
        kernel_compute1d_gravitational_contributions(systemInfos->NoktonsCount, noktonStates, noktonContributions);
        break;
    default:
        break;
    }
}

void kernel_compute_electric_contributions(const SystemInfos *systemInfos, const NoktonState *noktonStates, KernelNoktonContribution *noktonContributions)
{
    switch (systemInfos->Dimension)
    {
    case SYSTEM_DIMENSION_1D:
        kernel_compute1d_electric_contributions(systemInfos, noktonStates, noktonContributions);
        break;
    default:
        break;
    }
}

void kernel_compute_contributions(const SystemInfos *systemInfos, const NoktonState *noktonStates, KernelNoktonContribution *noktonContributions)
{
    switch (systemInfos->SystemType)
    {
    case SYSTEM_TYPE_NO_ELECTRIC_CONTRIBUTIONS:
        kernel_compute_gravitational_contributions(systemInfos, noktonStates, noktonContributions);
        break;
    case SYSTEM_TYPE_NO_GRAVITATIONAL_CONTRIBUTIONS:
        kernel_compute1d_electric_contributions(systemInfos, noktonStates, noktonContributions);
        break;
    default:
        kernel_compute_gravitational_contributions(systemInfos, noktonStates, noktonContributions);
        kernel_compute_electric_contributions(systemInfos, noktonStates, noktonContributions);
        break;
    }
}

void kernel_compute_pulses(const SystemInfos *systemInfos, const KernelNoktonContribution *noktonContributions, NoktonState *noktonStates)
{
    switch (systemInfos->Dimension)
    {
    case SYSTEM_DIMENSION_1D:
        kernel_compute1d_pulses(systemInfos, noktonContributions, noktonStates);
        break;
    default:
        break;
    }
}

double kernel_compute_path_probabilities(const SystemInfos *systemInfos, const int time, const PathNoktonDisplacements *noktonPaths, const NoktonState *noktonStates)
{
    switch (systemInfos->Dimension)
    {
    case SYSTEM_DIMENSION_1D:
        return kernel_compute1d_path_probabilities(time, systemInfos->NoktonsCount, noktonPaths, noktonStates);
    default:
        return 1;
    }
}

void kernel_compute_state(const SystemInfos *systemInfos, const int time, const PathNoktonDisplacements *noktonPaths, KernelComputingState *kernelComputingState)
{
    kernelComputingState->PathProbability *= kernel_compute_path_probabilities(systemInfos, time, noktonPaths, kernelComputingState->NoktonStates);
    kernel_compute_positions(systemInfos, time, noktonPaths, kernelComputingState->NoktonStates);
    kernel_compute_contributions(systemInfos, kernelComputingState->NoktonStates, kernelComputingState->NoktonContributions);
    kernel_compute_pulses(systemInfos, kernelComputingState->NoktonContributions, kernelComputingState->NoktonStates);
}

void kernel_compute_path_state(const SystemInfos *systemInfos, const int width, const Path *path, KernelComputingState *kernelComputingState)
{
    for (int time = 0; time < width; time++)
    {
        kernel_compute_state(systemInfos, time, path->NoktonDisplacements, kernelComputingState);
    }
}

KernelPathComputingState kernel_compute_next_path(const SystemInfos *systemInfos, const int width, const int noktonIndex, const int time, Path *path)
{
    switch (systemInfos->Dimension)
    {
    case SYSTEM_DIMENSION_1D:
        return kernel_compute1d_next_path(systemInfos->PulseType, width, noktonIndex, time, path);
    default:
        return KERNEL_PATH_COMPUTING_STATE_DONE;
    }
}