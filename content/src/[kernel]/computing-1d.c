#include "../../headers/[kernel]/computing-1d.h"
#include "../../headers/[kernel]/computing.h"

void kernel_compute1d_position(const PathDisplacement PathDisplacement, NoktonState *noktonState)
{
    int PathDisplacementStep;

    switch (PathDisplacement)
    {
    case PATH_DISPLACEMENT_X_POSITIVE:
        PathDisplacementStep = 1;
        break;
    case PATH_DISPLACEMENT_X_NEGATIVE:
        PathDisplacementStep = -1;
        break;
    default:
        PathDisplacementStep = 0;
        break;
    }

    noktonState->XPosition += PathDisplacementStep;
}

void kernel_compute1d_positions(const int time, const int count, const PathNoktonDisplacements *noktonPaths, NoktonState *noktonStates)
{
    for (int i = count - 1; i >= 0; i--)
    {
        kernel_compute1d_position(noktonPaths[i].Displacements[time], &noktonStates[i]);
    }
}

void kernel_compute1d_distances(const NoktonState *noktonState1, const NoktonState *noktonState2, KernelDistance *distance)
{
    distance->X = kernel_compute_distance(noktonState1->XPosition, noktonState2->XPosition);
}

void kernel_compute1d_gravitational_contribution(const int noktonIndex, const int count, const NoktonState *noktonStates, KernelNoktonContribution *noktonContribution)
{
    KernelDistance distance;
    NoktonState noktonState1 = noktonStates[noktonIndex];
    double contribution;

    for (int i = count - 1; i >= 0; i--)
    {
        if (i != noktonIndex)
        {
            kernel_compute1d_distances(&noktonState1, &noktonStates[i], &distance);
            /* Compute contribution */
            if (distance.X != 0)
            {
                contribution = H_G / (distance.X * distance.X);
            }
            else
            {
                contribution = 0;
            }
            /* Set contribution */
            if (distance.X > 0)
            {
                noktonContribution->XNegative += contribution;
            }
            else
            {
                if (distance.X < 0)
                {
                    noktonContribution->XPositive += contribution;
                }
            }
        }
    }
}

void kernel_compute1d_gravitational_contributions(const int count, const NoktonState *noktonStates, KernelNoktonContribution *noktonContributions)
{
    KernelNoktonContribution noktonContribution;

    for (int i = count - 1; i >= 0; i--)
    {
        noktonContribution.XPositive = 0;
        noktonContribution.XNegative = 0;

        kernel_compute1d_gravitational_contribution(i, count, noktonStates, &noktonContribution);
        noktonContributions[i] = noktonContribution;
    }
}

void kernel_compute1d_electric_contribution(const SystemInfos *systemInfos, const int noktonIndex, const NoktonState *noktonStates, KernelNoktonContribution *noktonContribution)
{
    KernelDistance distance;
    NoktonType noktonType1 = systemInfos->NoktonTypes[noktonIndex];
    NoktonState noktonState1 = noktonStates[noktonIndex];
    int noktonTypesCoupling;
    double contribution;

    for (int i = systemInfos->NoktonsCount - 1; i >= 0; i--)
    {
        if (i != noktonIndex)
        {
            kernel_compute1d_distances(&noktonState1, &noktonStates[i], &distance);
            /* Compute contribution */
            if (distance.X != 0)
            {
                contribution = H_E / (distance.X * distance.X);
            }
            else
            {
                contribution = 0;
            }
            /* Set contribution */
            noktonTypesCoupling = noktonType1 * systemInfos->NoktonTypes[i] * distance.X;

            if (noktonTypesCoupling < 0)
            {
                noktonContribution->XNegative += contribution;
            }
            else
            {
                if (noktonTypesCoupling > 0)
                {
                    noktonContribution->XPositive += contribution;
                }
            }
        }
    }
}

void kernel_compute1d_electric_contributions(const SystemInfos *systemInfos, const NoktonState *noktonStates, KernelNoktonContribution *noktonContributions)
{
    KernelNoktonContribution noktonContribution;

    for (int i = systemInfos->NoktonsCount - 1; i >= 0; i--)
    {
        noktonContribution.XPositive = 0;
        noktonContribution.XNegative = 0;

        kernel_compute1d_electric_contribution(systemInfos, i, noktonStates, &noktonContribution);
        noktonContributions[i] = noktonContribution;
    }
}

void kernel_compute1d_pulse(const SystemPulseType pulseType, const KernelNoktonContribution *noktonContribution, NoktonState *noktonState)
{
    double contributionsSum = 1.0f / (1.0f + noktonContribution->XPositive + noktonContribution->XNegative);
    noktonState->XPositivePulse = (noktonState->XPositivePulse + noktonContribution->XPositive) * contributionsSum;
    noktonState->XNegativePulse = (noktonState->XNegativePulse + noktonContribution->XNegative) * contributionsSum;

    if (pulseType == SYSTEM_PULSE_TYPE_NONE)
    {
        noktonState->NullPulse = 1 - (noktonState->XPositivePulse + noktonState->XNegativePulse);
    }
}

void kernel_compute1d_pulses(const SystemInfos *systemInfos, const KernelNoktonContribution *noktonContributions, NoktonState *noktonStates)
{
    for (int i = systemInfos->NoktonsCount - 1; i >= 0; i--)
    {
        kernel_compute1d_pulse(systemInfos->PulseType, &noktonContributions[i], &noktonStates[i]);
    }
}

double kernel_compute1d_path_probability(const PathDisplacement PathDisplacement, const NoktonState *noktonState)
{
    double probability;

    switch (PathDisplacement)
    {
    case PATH_DISPLACEMENT_X_POSITIVE:
        probability = noktonState->XPositivePulse;
        break;
    case PATH_DISPLACEMENT_X_NEGATIVE:
        probability = noktonState->XNegativePulse;
        break;
    default:
        probability = noktonState->NullPulse;
        break;
    }

    return probability;
}

double kernel_compute1d_path_probabilities(const int time, const int count, const PathNoktonDisplacements *noktonPaths, const NoktonState *noktonStates)
{
    double probability = 1;

    for (int i = count - 1; i >= 0; i--)
    {
        probability *= kernel_compute1d_path_probability(noktonPaths[i].Displacements[time], &noktonStates[i]);
    }

    return probability;
}

KernelPathComputingState kernel_compute1d_next_path(const SystemPulseType pulseType, const int width, const int noktonIndex, const int time, Path *path)
{
    PathDisplacement currentPathDisplacement = path->NoktonDisplacements[noktonIndex].Displacements[time];
    PathDisplacement nextPathDisplacement;

    if (currentPathDisplacement != PATH_DISPLACEMENT_X_NEGATIVE)
    {
        if (pulseType == SYSTEM_PULSE_TYPE_NONE)
        {
            switch (currentPathDisplacement)
            {
            case PATH_DISPLACEMENT_NONE:
                nextPathDisplacement = PATH_DISPLACEMENT_X_POSITIVE;
                break;
            default:
                nextPathDisplacement = PATH_DISPLACEMENT_X_NEGATIVE;
                break;
            }
        }
        else
        {
            nextPathDisplacement = PATH_DISPLACEMENT_X_NEGATIVE;
        }

        path->NoktonDisplacements[noktonIndex].Displacements[time] = nextPathDisplacement;
        return KERNEL_PATH_COMPUTING_STATE_IN_PROGRESS;
    }
    else
    {
        path->NoktonDisplacements[noktonIndex].Displacements[time] = pulseType == SYSTEM_PULSE_TYPE_NONE ? PATH_DISPLACEMENT_NONE : PATH_DISPLACEMENT_X_POSITIVE;
        int nextTime = time - 1;

        if (nextTime >= 0)
        {
            return kernel_compute1d_next_path(pulseType, width, noktonIndex, nextTime, path);
        }
        else
        {
            nextTime = width - 1;
            int nextNoktonIndex = noktonIndex - 1;

            if (nextNoktonIndex >= 0)
            {
                return kernel_compute1d_next_path(pulseType, width, nextNoktonIndex, nextTime, path);
            }
            else
            {
                return KERNEL_PATH_COMPUTING_STATE_DONE;
            }
        }
    }
}