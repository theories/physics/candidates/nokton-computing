#include <math.h>
#include "../../headers/[observables]/position-1d.h"

void observables_compute1d_final_position_mean(const int count, const KernelComputingState *state, ObservablesFinalPosition *means)
{
    double pathProbability = state->PathProbability;

    for (int i = count - 1; i >= 0; i--)
    {
        means[i].XPosition += state->NoktonStates[i].XPosition * pathProbability;
    }
}

void observables_compute1d_final_position_stddev(const int count, const KernelComputingState *state, ObservablesFinalPosition *stddevs)
{
    double pathProbability = state->PathProbability;

    for (int i = count - 1; i >= 0; i--)
    {
        stddevs[i].XPosition += state->NoktonStates[i].XPosition * state->NoktonStates[i].XPosition * pathProbability;
    }
}

void observables_finalize1d_final_position_stddev(const int count, const ObservablesFinalPosition *means, ObservablesFinalPosition *stddevs)
{
    for (int i = count - 1; i >= 0; i--)
    {
        stddevs[i].XPosition = sqrt(stddevs[i].XPosition - means[i].XPosition * means[i].XPosition);
    }
}