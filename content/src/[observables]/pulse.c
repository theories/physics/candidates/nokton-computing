#include "../../headers/[observables]/pulse.h"
#include "../../headers/[observables]/pulse-1d.h"

void observables_compute_final_pulse_mean(const SystemInfos *systemInfos, const KernelComputingState *state, ObservablesFinalPulse *means)
{
    switch (systemInfos->Dimension)
    {
    case SYSTEM_DIMENSION_1D:
        observables_compute1d_final_pulse_mean(systemInfos->NoktonsCount, state, means);
        break;
    default:
        break;
    }
}