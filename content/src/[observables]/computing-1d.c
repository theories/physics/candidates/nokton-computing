#include "../../headers/[observables]/computing-1d.h"

void observables_initialize1d_path_computing(const int count, const NoktonState *initialNoktonStates, KernelComputingState *kernelComputingState)
{
    NoktonState sourceNoktonState;
    NoktonState targetNoktonState;

    for (int i = count - 1; i >= 0; i--)
    {
        sourceNoktonState = initialNoktonStates[i];

        targetNoktonState.XPosition = sourceNoktonState.XPosition;
        targetNoktonState.YPosition = sourceNoktonState.YPosition;
        targetNoktonState.XPositivePulse = sourceNoktonState.XPositivePulse;
        targetNoktonState.XNegativePulse = sourceNoktonState.XNegativePulse;
        targetNoktonState.NullPulse = sourceNoktonState.NullPulse;

        kernelComputingState->NoktonStates[i] = targetNoktonState;
    }

    kernelComputingState->PathProbability = 1;
}