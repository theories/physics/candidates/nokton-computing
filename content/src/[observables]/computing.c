#include <math.h>
#include "../../headers/[observables]/computing.h"
#include "../../headers/[observables]/computing-1d.h"
#include "../../headers/[observables]/position.h"
#include "../../headers/[observables]/pulse.h"
#include "../../headers/[observables]/mass.h"
#include "../../headers/[kernel]/computing.h"
#include "../../headers/[progress]/progress-step-type.h"
#include "../../headers/[progress]/progress.h"

void observables_kernel_compute(const SystemInfos *systemInfos, const int kernelsCount, const int width, const Path *path, KernelComputingState *kernelComputingState, ObservablesComputingState *observablesComputingState)
{
    kernel_compute_path_state(systemInfos, width, path, kernelComputingState);

    if (systemInfos->ObservableStates.FinalPosition == CONFIG_OBSERVABLE_STATE_ENABLED || kernelsCount > 1)
    {
        observables_compute_final_position_mean(systemInfos, kernelComputingState, observablesComputingState->FinalPositionMeans);

        if (systemInfos->ObservableStates.FinalPosition == CONFIG_OBSERVABLE_STATE_ENABLED)
        {
            observables_compute_final_position_stddev(systemInfos, kernelComputingState, observablesComputingState->FinalPositionStddevs);
        }
    }

    if (systemInfos->ObservableStates.FinalPulse == CONFIG_OBSERVABLE_STATE_ENABLED || kernelsCount > 1)
    {
        observables_compute_final_pulse_mean(systemInfos, kernelComputingState, observablesComputingState->FinalPulseMeans);
    }

    if (systemInfos->ObservableStates.FinalMass == CONFIG_OBSERVABLE_STATE_ENABLED)
    {
        observables_compute_final_mass_mean(systemInfos, width, kernelComputingState, path, observablesComputingState->FinalMassMeans);
    }
}

void observables_kernel_finalize(const SystemInfos *systemInfos, ObservablesComputingState *observablesComputingState)
{
    if (systemInfos->ObservableStates.FinalPosition == CONFIG_OBSERVABLE_STATE_ENABLED)
    {
        observables_finalize_final_position_stddev(systemInfos, observablesComputingState->FinalPositionMeans, observablesComputingState->FinalPositionStddevs);
    }
}

void observables_compute_initial_nokton_states(const SystemInfos *systemInfos, const int width, const int kernelIndex, const ObservablesComputingState *observablesComputingState, const NoktonState *initialNoktonStates, NoktonState *newInitialNoktonStates, Path *path)
{
    for (int i = systemInfos->NoktonsCount - 1; i >= 0; i--)
    {
        newInitialNoktonStates[i] = initialNoktonStates[i];

        for (int time = width - 1; time >= 0; time--)
        {
            switch (systemInfos->Dimension)
            {
            case SYSTEM_DIMENSION_1D:
                path->NoktonDisplacements[i].Displacements[time] = systemInfos->PulseType == SYSTEM_PULSE_TYPE_NO_NULL ? PATH_DISPLACEMENT_X_POSITIVE : PATH_DISPLACEMENT_NONE;
                break;
            default:
                break;
            }
        }
    }
}

void observables_initialize_path_computing(const SystemInfos *systemInfos, const NoktonState *initialNoktonStates, KernelComputingState *kernelComputingState)
{
    switch (systemInfos->Dimension)
    {
    case SYSTEM_DIMENSION_1D:
        observables_initialize1d_path_computing(systemInfos->NoktonsCount, initialNoktonStates, kernelComputingState);
        break;
    default:
        break;
    }
}

void observables_compute(const SystemInfos *systemInfos, const int width, const int kernelsCount, const NoktonState *initialNoktonStates, ObservablesComputingState *observablesComputingState, ProgressInfos *progressInfos)
{
    // Computing.
    NoktonState newInitialNoktonStates[MAX_NOKTONS_COUNT];
    SystemDimension dimension = systemInfos->Dimension;
    SystemPulseType pulseType = systemInfos->PulseType;

    progressInfos->KernelsCount = kernelsCount;

    int dimensionBase;

    switch (dimension)
    {
    case SYSTEM_DIMENSION_1D:
        dimensionBase = pulseType == SYSTEM_PULSE_TYPE_NONE ? 3 : 2;
        break;
    case SYSTEM_DIMENSION_2D:
        dimensionBase = pulseType == SYSTEM_PULSE_TYPE_NONE ? 5 : 4;
        break;
    default:
        dimensionBase = pulseType == SYSTEM_PULSE_TYPE_NONE ? 7 : 6;
        break;
    }

    int count = systemInfos->NoktonsCount;

    progressInfos->KernelTotalPaths = pow(dimensionBase, count * width);
    progressInfos->CurrentKernel = 0;
    progressInfos->KernelCurrentPath = 0;

    time(&progressInfos->Start);
    time(&progressInfos->StepStart);

    progress_show(PROGRESS_STEP_TYPE_STARTING, progressInfos);

    KernelComputingState kernelComputingState;
    Path path;

    for (int i = 0; i < kernelsCount; i++)
    {
        progressInfos->CurrentKernel = i;
        observables_compute_initial_nokton_states(systemInfos, width, i, observablesComputingState, initialNoktonStates, newInitialNoktonStates, &path);
        progressInfos->KernelCurrentPath = 0;

        do
        {
            progressInfos->KernelCurrentPath++;
            observables_initialize_path_computing(systemInfos, newInitialNoktonStates, &kernelComputingState);
            observables_kernel_compute(systemInfos, kernelsCount, width, &path, &kernelComputingState, observablesComputingState);
            progress_show(PROGRESS_STEP_TYPE_COMPUTING, progressInfos);
        } while (kernel_compute_next_path(systemInfos, width, count - 1, width - 1, &path) == KERNEL_PATH_COMPUTING_STATE_IN_PROGRESS);
    }

    observables_kernel_finalize(systemInfos, observablesComputingState);

    progressInfos->CurrentKernel++;
    time(&progressInfos->End);
    progress_show(PROGRESS_STEP_TYPE_ENDING, progressInfos);
}