#include "../../headers/[observables]/pulse-1d.h"

void observables_compute1d_final_pulse_mean(const int count, const KernelComputingState *state, ObservablesFinalPulse *means)
{
    double pathProbability = state->PathProbability;
    ObservablesFinalPulse mean;
    NoktonState noktonState;

    for (int i = count - 1; i >= 0; i--)
    {
        mean = means[i];
        noktonState = state->NoktonStates[i];

        mean.XPositivePulse += noktonState.XPositivePulse * pathProbability;
        mean.XNegativePulse += noktonState.XNegativePulse * pathProbability;
        mean.NullPulse += noktonState.NullPulse * pathProbability;

        means[i] = mean;
    }
}