#include "../../headers/[observables]/position.h"
#include "../../headers/[observables]/position-1d.h"

void observables_compute_final_position_mean(const SystemInfos *systemInfos, const KernelComputingState *state, ObservablesFinalPosition *means)
{
    switch (systemInfos->Dimension)
    {
    case SYSTEM_DIMENSION_1D:
        observables_compute1d_final_position_mean(systemInfos->NoktonsCount, state, means);
        break;
    default:
        break;
    }
}

void observables_compute_final_position_stddev(const SystemInfos *systemInfos, const KernelComputingState *state, ObservablesFinalPosition *stddevs)
{
    switch (systemInfos->Dimension)
    {
    case SYSTEM_DIMENSION_1D:
        observables_compute1d_final_position_stddev(systemInfos->NoktonsCount, state, stddevs);
        break;
    default:
        break;
    }
}

void observables_finalize_final_position_stddev(const SystemInfos *systemInfos, const ObservablesFinalPosition *means, ObservablesFinalPosition *stddevs)
{
    switch (systemInfos->Dimension)
    {
    case SYSTEM_DIMENSION_1D:
        observables_finalize1d_final_position_stddev(systemInfos->NoktonsCount, means, stddevs);
        break;
    default:
        break;
    }
}