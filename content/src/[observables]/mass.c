#include "../../headers/[observables]/mass.h"
#include "../../headers/[observables]/mass-1d.h"

void observables_compute_final_mass_mean(const SystemInfos *systemInfos, const int width, const KernelComputingState *state, const Path *path, double *means)
{
    switch (systemInfos->Dimension)
    {
    case SYSTEM_DIMENSION_1D:
        observables_compute1d_final_mass_mean(systemInfos->NoktonsCount, width, state, path, means);
        break;
    default:
        break;
    }
}