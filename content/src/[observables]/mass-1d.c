#include "../../headers/[observables]/mass-1d.h"

void observables_compute1d_final_mass_mean(const int count, const int width, const KernelComputingState *state, const Path *path, double *means)
{
    double pathProbability = state->PathProbability;
    PathNoktonDisplacements displacements;

    for (int i = count - 1; i >= 0; i--)
    {
        displacements = path->NoktonDisplacements[i];
        if (displacements.Displacements[width - 1] == PATH_DISPLACEMENT_NONE)
        {
            means[i] += pathProbability;
        }
    }
}