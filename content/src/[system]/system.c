#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "../../headers/(...)/constants.h"
#include "../../headers/[nokton]/state.h"
#include "../../headers/[observables]/computing.h"
#include "../../headers/[observables]/computing-state.h"
#include "../../headers/[system]/infos.h"
#include "../../headers/[system]/system.h"
#include "../../headers/[system]/initialization.h"
#include "../../headers/[output]/writing.h"
#include "../../headers/[output]/output.h"
#include "../../headers/[config]/fetching.h"
#include "../../headers/[config]/config.h"

SystemInitialization system_initialize_infos(const char *configFile, SystemInfos *systemInfos, NoktonState *initialNoktonStates, int *width, int *kernelsCount, char *error)
{
    // Default values.
    systemInfos->Dimension = SYSTEM_DIMENSION_1D;
    systemInfos->SystemType = SYSTEM_TYPE_NONE;
    systemInfos->PulseType = SYSTEM_PULSE_TYPE_NO_NULL;
    systemInfos->NoktonsCount = 0;
    systemInfos->ObservableStates.FinalPosition = CONFIG_OBSERVABLE_STATE_DISABLED;
    systemInfos->ObservableStates.FinalPulse = CONFIG_OBSERVABLE_STATE_DISABLED;
    systemInfos->ObservableStates.FinalMass = CONFIG_OBSERVABLE_STATE_DISABLED;

    *width = 1;
    *kernelsCount = 1;
    // Open file.
    FILE *file = fopen(configFile, "r");

    if (file == NULL)
    {
        sprintf(error, "Can not open file '%s' for reading", configFile);
        return SYSTEM_INITIALIZATION_FAILURE;
    }
    // Read configuration.
    char line[MAX_CONFIG_LINE_LENGTH];
    char trimLine[MAX_CONFIG_LINE_LENGTH];
    char comment[MAX_CONFIG_LINE_LENGTH];
    char key[MAX_CONFIG_LINE_LENGTH];
    char trimKey[MAX_CONFIG_LINE_LENGTH];
    char value[MAX_CONFIG_LINE_LENGTH];
    char trimValue[MAX_CONFIG_LINE_LENGTH];
    int maxFetchingLine = MAX_CONFIG_LINE_LENGTH - 1;
    ConfigFetching readingNewLine;
    ConfigFetching readingKeyValue;
    int readingNoktonProperties = 0;
    NoktonState *noktonState;
    char innerError[MAX_ERROR_LENGTH];

    do
    {
        readingNewLine = config_read_line(file, maxFetchingLine, line);

        if (config_is_config(line, maxFetchingLine) == CONFIG_FETCHING_IS_CONFIG)
        {
            config_trim_line_string(line, trimLine);

            if (strlen(trimLine) > 0)
            {
                if (config_get_key_value(trimLine, maxFetchingLine, key, value) == CONFIG_FETCHING_UNFOUND_VALUE)
                {
                    sprintf(error, "Unfound value in line '%s'", line);
                    return SYSTEM_INITIALIZATION_FAILURE;
                }

                config_trim_line_string(key, trimKey);
                config_trim_line_string(value, trimValue);

                if (strlen(trimKey) == 0)
                {
                    sprintf(error, "Empty key in line '%s'", line);
                    return SYSTEM_INITIALIZATION_FAILURE;
                }

                if (strlen(trimValue) == 0)
                {
                    sprintf(error, "Empty value in line '%s'", line);
                    return SYSTEM_INITIALIZATION_FAILURE;
                }

                if (strcmp(trimKey, "NoktonType") == 0)
                {
                    if (systemInfos->NoktonsCount >= MAX_NOKTONS_COUNT)
                    {
                        sprintf(error, "Max noktons count is reached from line '%s'", line);
                        return SYSTEM_INITIALIZATION_FAILURE;
                    }

                    NoktonType noktonType;
                    int isValidNoktonType = 1;

                    if (strcmp(trimValue, "NEUTRAL") == 0)
                    {
                        noktonType = NOKTON_TYPE_NEUTRAL;
                    }
                    else if (strcmp(trimValue, "POSITIVE") == 0)
                    {
                        noktonType = NOKTON_TYPE_POSITIVE;
                    }
                    else if (strcmp(trimValue, "NEGATIVE") == 0)
                    {
                        noktonType = NOKTON_TYPE_NEGATIVE;
                    }
                    else
                    {
                        readingKeyValue = CONFIG_FETCHING_UNKNOWN_VALUE;
                        isValidNoktonType = 0;
                    }

                    if (isValidNoktonType)
                    {
                        systemInfos->NoktonTypes[systemInfos->NoktonsCount] = noktonType;
                        noktonState = &initialNoktonStates[systemInfos->NoktonsCount];

                        noktonState->XPosition = 0;
                        noktonState->XPosition = 0;
                        noktonState->XPosition = 0;

                        noktonState->XPositivePulse = 0.0f;
                        noktonState->XNegativePulse = 0.0f;
                        noktonState->YPositivePulse = 0.0f;
                        noktonState->YNegativePulse = 0.0f;
                        noktonState->ZPositivePulse = 0.0f;
                        noktonState->ZNegativePulse = 0.0f;

                        systemInfos->NoktonsCount++;
                        readingNoktonProperties = 1;
                    }
                }
                else
                {
                    if (readingNoktonProperties)
                    {
                        readingKeyValue = config_initialize_nokton(trimKey, trimValue, noktonState, innerError);
                    }
                    else
                    {
                        readingKeyValue = config_initialize_properties(trimKey, trimValue, systemInfos, width, kernelsCount, innerError);
                    }
                }

                if (readingKeyValue == CONFIG_FETCHING_UNKNOWN_KEY)
                {
                    sprintf(error, "Unknown key in line '%s'", line);
                    return SYSTEM_INITIALIZATION_FAILURE;
                }

                if (readingKeyValue == CONFIG_FETCHING_UNKNOWN_VALUE)
                {
                    sprintf(error, "Unknown value in line '%s'", line);
                    return SYSTEM_INITIALIZATION_FAILURE;
                }

                if (readingKeyValue == CONFIG_FETCHING_UNFOUND_VALUE)
                {
                    sprintf(error, "Unfound value in line '%s'", line);
                    return SYSTEM_INITIALIZATION_FAILURE;
                }

                if (readingKeyValue == CONFIG_FETCHING_VALUE_OUT_OF_RANGE)
                {
                    sprintf(error, "Value out of range in line '%s' - %s", line, innerError);
                    return SYSTEM_INITIALIZATION_FAILURE;
                }
            }
        }
    } while (readingNewLine == CONFIG_FETCHING_MORE_LINES);
    // Set Null-Pulses.
    NoktonState *tmpNoktonState;

    for (int i = 0; i < systemInfos->NoktonsCount; i++)
    {
        tmpNoktonState = &initialNoktonStates[i];
        tmpNoktonState->NullPulse = 1 - (tmpNoktonState->XNegativePulse + tmpNoktonState->XPositivePulse +
                                         tmpNoktonState->YNegativePulse + tmpNoktonState->YPositivePulse +
                                         tmpNoktonState->ZNegativePulse + tmpNoktonState->ZPositivePulse);

        if (tmpNoktonState->NullPulse < 0 || tmpNoktonState->NullPulse > 1)
        {
            sprintf(error, "Null pulse value '%f' is out of range for nokton #%d - Excepted value between 0 and 1", tmpNoktonState->NullPulse, i + 1);
            return SYSTEM_INITIALIZATION_FAILURE;
        }
    }

    fclose(file);
    return SYSTEM_INITIALIZATION_SUCCESS;
}

SystemComputing system_compute(const char *configFile, const char *outputFile, char *error)
{
    SystemInfos systemInfos;
    NoktonState initialNoktonStates[MAX_NOKTONS_COUNT];
    int width;
    int kernelsCount;

    if (system_initialize_infos(configFile, &systemInfos, initialNoktonStates, &width, &kernelsCount, error) != SYSTEM_INITIALIZATION_SUCCESS)
    {
        return SYSTEM_COMPUTING_FAILURE;
    }

    ObservablesComputingState observablesComputingState;
    system_initialize_observables_computing_state(systemInfos.NoktonsCount, &observablesComputingState);

    ProgressInfos progressInfos;
    observables_compute(&systemInfos, width, kernelsCount, initialNoktonStates, &observablesComputingState, &progressInfos);
    if (output_write(outputFile, &systemInfos, width, kernelsCount, initialNoktonStates, &observablesComputingState, error) != OUTPUT_WRITING_SUCCESS)
    {
        return SYSTEM_COMPUTING_FAILURE;
    }

    return SYSTEM_COMPUTING_SUCCESS;
}

void system_initialize_observables_computing_state(const int count, ObservablesComputingState *computingState)
{
    ObservablesFinalPulse finalPulseMean;

    for (int i = count - 1; i >= 0; i--)
    {
        computingState->FinalPositionMeans[i].XPosition = 0;
        computingState->FinalPositionStddevs[i].XPosition = 0;

        finalPulseMean.XPositivePulse = 0;
        finalPulseMean.XNegativePulse = 0;
        finalPulseMean.NullPulse = 0;

        computingState->FinalPulseMeans[i] = finalPulseMean;
        computingState->FinalMassMeans[i] = 0;
    }
}