#include <string.h>
#include <stdio.h>
#include "../../headers/(...)/constants.h"
#include "../../headers/[arguments]/arguments.h"

ArgumentsFetching arguments_fetch(const int argc, const char **argv, char *configFile, char *outputFile, char *error)
{
    // Fetch arguments.
    const char *arg;
    int hasConfig = 0;
    int hasOutput = 0;
    int argLength;

    for (int i = 1; i < argc; i++)
    {
        arg = argv[i];
        // Help.
        if (strcmp(arg, "-h") == 0 || strcmp(arg, "--help") == 0)
        {
            printf("Usage: nokton-computing [-h|--help] [-c] [config-file] [-o] [output-file]\n");
            return ARGUMENTS_FETCHING_EXIT;
        }

        if (strcmp(arg, "-c") == 0)
        {
            // Config file.
            i++;

            if (i < argc)
            {
                arg = argv[i];
                argLength = strlen(arg);
                strncat(configFile, argv[i], argLength < MAX_FILE_PATH - 1 ? argLength : MAX_FILE_PATH - 1);
                hasConfig = 1;
            }
            else
            {
                strcpy(error, "Excepted config file name");
                return ARGUMENTS_FETCHING_FAILURE;
            }
        }
        else if (strcmp(arg, "-o") == 0)
        {
            // Output file.
            i++;

            if (i < argc)
            {
                hasOutput = 1;
                strcpy(outputFile, argv[i]);
            }
            else
            {
                strcpy(error, "Excepted output file name");
                return ARGUMENTS_FETCHING_FAILURE;
            }
        }
        else
        {
            // Unknown option.
            sprintf(error, "Unkown option '%s'", arg);
            return ARGUMENTS_FETCHING_FAILURE;
        }
    }
    // Default values.
    if (!hasConfig)
    {
        strcpy(configFile, "system.conf");
    }

    if (!hasOutput)
    {
        strcpy(outputFile, "computing.out");
    }

    return ARGUMENTS_FETCHING_SUCCESS;
}